<?php

$router->group(['prefix' => 'coding'], function() use ($router) {
    $router->post('convert', ['as' => 'convert', 'uses' => 'CodingController@convert']);
    $router->post('palindrome', ['as' => 'palindrome', 'uses' => 'CodingController@palindrome']);
});

$router->group(['prefix' => 'query'], function() use ($router) {
    $router->post('transaction', ['as' => 'transaction', 'uses' => 'QueryController@transaction']);
});
