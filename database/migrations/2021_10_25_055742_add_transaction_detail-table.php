<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransactionDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_m_transaction_detail', function (Blueprint $table) {
            $table->increments('transaction_detail_id');
            $table->unsignedInteger('transaction_id');
            $table->unsignedInteger('harga')->nullable();
            $table->unsignedInteger('jumlah')->nullable();
            $table->unsignedInteger('sub_total')->nullable();

            // $table->foreign('transaction_id')->references('transaction_id')->on('tbl_m_transaction');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_m_transaction_detail');
    }
}
