<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_m_transaction', function (Blueprint $table) {
            $table->increments('transaction_id');
            $table->date('tanggal_order')->nullable();
            $table->unsignedInteger('status_pelunasan')->default(0);
            $table->date('tanggal_pelunasan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_m_transaction');
    }
}
