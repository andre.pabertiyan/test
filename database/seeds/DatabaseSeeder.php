<?php

use Illuminate\Database\Seeder;

use App\Models\Transaction;
use App\Models\TransactionDetail;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('TransactionTableSeeder');
        $this->command->info('Transaction table seeded!');

        $this->call('TransactionDetaillTableSeeder');
        $this->command->info('Transaction Detail table seeded!');
    }
}

class TransactionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tbl_m_transaction')->delete();

        Transaction::create(
            [
                'tanggal_order' => '2020-12-01 11:30:00',
                'status_pelunasan' => '1',
                'tanggal_pelunasan' => '2020-12-01 12:00:00',
            ]
        );

        Transaction::create(
            [
                'tanggal_order' => '2020-12-02 10:30:00',
                'status_pelunasan' => '0',
                'tanggal_pelunasan' => null,
            ]
        );
    }

}

class TransactionDetaillTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tbl_m_transaction_detail')->delete();

        TransactionDetail::create(
            [
                'transaction_id' => 1,
                'harga' => 10000,
                'jumlah' => 2,
                'sub_total' => 20000,
            ]
        );

        TransactionDetail::create(
            [
                'transaction_id' => 2,
                'harga' => 12500,
                'jumlah' => 4,
                'sub_total' => 25000,
            ]
        );
    }

}
