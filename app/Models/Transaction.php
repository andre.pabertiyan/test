<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\AuditingTrait;

class Transaction extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'transaction_id';
    protected $table = 'tbl_m_transaction';

    public function details() {
        return $this->hasMany('App\Models\TransactionDetail', 'transaction_id');
    }
}