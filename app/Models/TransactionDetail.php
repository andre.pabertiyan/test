<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\AuditingTrait;

class TransactionDetail extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'transaction_detail_id';
    protected $table = 'tbl_m_transaction_detail';
}