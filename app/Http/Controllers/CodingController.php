<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\QueryException;

class CodingController extends Controller {
	public function convert(Request $req) {
		$ret = (object) [];
		$ret->result = true;
        $ret->msg = ''; 
		$ret->data = [];	

		try {
			if($req->param == 'dectobin') {
				$ret->data = [
					'decimal_value' => $this->dectobin($req->value),
					'binary_value' => $req->value,
				];
			} else {
				$ret->data = [
					'decimal_value' => $this->bintodec($req->value),
					'binary_value' => $req->value,
				];
			}
		} catch (\Exception $e) {
			$ret->result = false;
			$ret->msg = $e->getMessage();
		}
		return response()->json($ret);
	}

	public function palindrome(Request $req) {
		$ret = (object) [];
		$ret->result = true;
        $ret->msg = ''; 
		$ret->data = [];	

		try {
			$ret->data = [
				'original_string' => $req->param,
				'palindrome' => $this->checkPalindrome($req->param)
			];
		} catch (\Exception $e) {
			$ret->result = false;
			$ret->msg = $e->getMessage();
		}
		return response()->json($ret);
	}

	function bintodec($param) {
		$digits = str_split($param);
	    $reversed = array_reverse($digits);
	    $res = 0;

	    for($x=0; $x < count($reversed); $x++) {
	        if($reversed[$x] == 1) {
	            $res += pow(2, $x);
	        }
	    }

	    return $res;
	}

	function dectobin($param) {
		$int_val = '';
		while ($param >= 1){
	    	$int_vals = $param % 2;
	    	$param = round($param / 2, 0, PHP_ROUND_HALF_DOWN);
	    	$int_val .= $int_vals;
		}
		return strrev($int_val);
	}

	function checkPalindrome($param) {
		$split = str_split($param);
		$total = strlen($param);
		$text = '';

		for($i = ($total - 1); $i > 0; $i--) {
			$text .= $split[$i];
		}
		dd($text);
  		
  		if(strtolower($param) == strtolower($text)) {

  		} else {

  		}
	}
}