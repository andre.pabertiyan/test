<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\QueryException;

use App\Models\Transaction;

class QueryController extends Controller {
	public function transaction(Request $req) {
		$ret = (object) [];
		$ret->result = true;
        $ret->msg = ''; 
		$ret->data = [];	

		try {
			$list = Transaction::whereBetween(DB::raw('DATE(tanggal_order)'), [$req->date_start, $req->date_end])->with('details')->get();
			foreach ($list as $key => $value) {
				$total = 0;
				$jumlah = 0;
				foreach ($value->details as $keys => $row) {
					$total += $row->sub_total;
					$jumlah += $row->jumlah;
				}
				$ret->data[] = [
					'id' => $key+1,
					'tanggal_order' => $value->tanggal_order,
					'status' => $value->status_pelunasan == 1 ? 'lunas' : 'pending',
					'tanggal_pembayaran' => $value->tanggal_pelunasan,
					'total' => $total,
					'jumlah_barang' => $jumlah,
				];
			}
		} catch (\Exception $e) {
			$ret->result = false;
			$ret->msg = $e->getMessage();
		}
		return response()->json($ret);
	}
}